// console.log("Hello World");

let trainer = {
	name: 'Ash Ketchum',
	age: 10,
	Pokemon: ["Pikachu", "Charizard", "Squirtle", "Balbasaur"],
	Friends: { 
		hoen: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	}
}

trainer.talk = function(){
	console.log("Pikachu I choose you!");
}

console.log(trainer);

console.log("Result from dot notation:");
console.log(trainer.name);
console.log("Result from square bracket notation:");
console.log(trainer['Pokemon']);
console.log("Result from talk method:");
trainer.talk();

// ---

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.tackle = function(target){

		this.newHealth = this.health = target.health - this.attack;
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s' health is now reduced to " + this.newHealth);

		this.faint = function(){
		console.log(target.name + " fainted");
		}

		if (this.newHealth <= 0 ){
				this.faint();	
		} 
	}

	}

let pikachu = new Pokemon('Pikachu', 12 );
console.log(pikachu);
let geodude = new Pokemon('Geodude', 8);
console.log(geodude)
let mewtwo = new Pokemon('Mewtwo', 100);
console.log (mewtwo);

geodude.tackle(pikachu);
mewtwo.tackle(geodude);

